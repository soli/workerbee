"""Analyzing databases of Hive games.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import re
import sqlite3
import urllib.request
from concurrent.futures import ThreadPoolExecutor
from tempfile import TemporaryFile
from typing import Any, Optional
from zipfile import ZipFile

from .bee import add_movelist, add_moves, hash_board, pretty_print_board
from .boardspace import parse_boardspace


def get_con() -> sqlite3.Connection:
    """Get a connection to the database."""
    con = sqlite3.connect("data/games.db")
    con.execute(
        "CREATE TABLE IF NOT EXISTS positions(hash, move, white, black, result, date, filename, nexthash, nextmove)"
    )
    con.execute("CREATE INDEX IF NOT EXISTS idx_hash ON positions(hash)")
    return con


def is_in_db(filename: str, con: Optional[sqlite3.Connection] = None) -> bool:
    """Check for the presence of a given game in the DB."""
    if con is None:
        con = get_con()
        close = True
    else:
        close = False
    result = (
        con.execute(
            f"SELECT * FROM positions WHERE filename='{filename.split('/')[-1]}'"
        ).fetchone()
        is not None
    )
    if close:
        con.close()
    return result


def add_bs_to_db(
    filename: str, data: Optional[str] = None, con: Optional[sqlite3.Connection] = None
) -> None:
    """Add the positions from an SGF game to our DB."""
    if con is None:
        con = get_con()
        close = True
    else:
        close = False

    shortname = filename.split("/")[-1]

    if is_in_db(filename, con):
        print(filename, "is already in the database.")
        return

    try:
        (w, b, r, d, m) = parse_boardspace(filename, data)
        boards = add_movelist(m[:32])[1:]  # Arbitrary cutoff here
        if not boards or len(boards) < 2:
            raise ValueError(f"Less than 2 moves found for {(w, b, r, d, m)}")
        hashes = list(map(hash_board, boards))
        keys = ((h, i + 1, hashes[i + 1], m[i + 1]) for i, h in enumerate(hashes[:-1]))
        insert = f"INSERT INTO positions VALUES(?, ?, '{w}', '{b}', '{r}', '{d}', '{shortname}', ?, ?)"
        con.executemany(insert, keys)
        con.commit()
        print(f"added {shortname} to the DB.")
    except sqlite3.OperationalError as e:
        print(f"Failed to add {filename} because of DB error {e}")
        close = True
    except Exception as e:
        print(f"Failed to add {filename} because of error {e}")

    if close:
        con.close()
        con = None


def get_files(directory: str = "/", arch: bool = False) -> None:
    """Get files from BoardSpace archive and put them into our DB."""
    sgf = re.compile(r"<a href=\"(T!HV-.*\.sgf)\">")
    if arch:
        archives = re.compile(r"<a href=\"(archive-.*)/\">")
    zipped = re.compile(r"<a href=\"(games-.*\.zip)\">")
    baseurl = "https://www.boardspace.net/hive/hivegames" + directory
    con = get_con()

    with urllib.request.urlopen(baseurl) as response:
        html = response.read().decode()
    with con:
        # parallel is good for network, but DB chokes a bit
        with ThreadPoolExecutor() as pool:
            for m in sgf.finditer(html):
                filename = m.group(1)
                if not is_in_db(filename, con):
                    pool.submit(download_and_add_file, baseurl, filename)
            for z in zipped.finditer(html):
                pool.submit(get_files_from_zip, baseurl + z.group(1))
            if arch:
                for a in archives.finditer(html):
                    pool.submit(get_files, f"/{a.group(1)}/")


def download_and_add_file(baseurl: str, filename: str) -> None:
    """Download a file and add it to the DB."""
    with urllib.request.urlopen(baseurl + filename) as response:
        data = response.read().decode()
        add_bs_to_db(filename, data)


def get_files_from_zip(url: str) -> None:
    """Download a zip archive and extract its files to the DB."""
    con = get_con()

    with urllib.request.urlopen(url) as web:
        content = web.read()

    with con:
        with TemporaryFile(suffix=".zip") as tmp:
            tmp.write(content)
            with ZipFile(tmp) as archive:
                for file in archive.namelist():
                    if file.startswith("T!HV-") and not is_in_db(file):
                        with archive.open(file) as f:
                            data = f.read().decode()
                            add_bs_to_db(file, data, con)


def query(
    movestr: str,
    fromdate: Optional[str] = None,
    todate: Optional[str] = None,
    white: Optional[str] = None,
    black: Optional[str] = None,
) -> list[tuple[str, int, str, str]]:
    """Query the DB for some position given by a movestring."""
    board = add_moves(movestr)[-1]
    pretty_print_board(board)
    q_hash = hash_board(board)
    con = get_con()
    query_str = "SELECT filename, move, result, nexthash FROM positions WHERE hash == ?"
    query_params: list[Any] = [q_hash]
    if fromdate is not None:
        query_str += " AND date >= datetime(?)"
        query_params += [fromdate]
    if todate is not None:
        query_str += " AND date < datetime(?)"
        query_params += [todate]
    if white is not None:
        query_str += " AND white == ?"
        query_params += [white]
    if black is not None:
        query_str += " AND black == ?"
        query_params += [black]
    # order by date Ascending since older games have more chances to be in entomology DB
    query_str += " ORDER BY nexthash, date ASC"
    with con:
        result = con.execute(query_str, query_params).fetchall()
        return result


def count_games(fromdate: str = "2000-01-01 00:00", todate: str = "now") -> int:
    """Count how many different games are in the DB."""
    con = get_con()

    with con:
        return con.execute(
            "SELECT COUNT(DISTINCT filename) FROM positions WHERE date >= datetime(?) AND date < datetime(?)",
            (fromdate, todate),
        ).fetchone()[0]
