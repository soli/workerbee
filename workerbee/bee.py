"""Analyzing databases of Hive games.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from itertools import accumulate
from typing import Optional
from zlib import compress

Board = dict[str, tuple[int, int, int]]
LBoard = list[tuple[str, tuple[int, int, int]]]


def add_move(board: Board, movestr: str) -> Board:
    """Add move from movestr to board and return the new board."""
    if movestr == "pass":
        return board

    if " " not in movestr:
        bug = movestr
        return {bug: (0, 0, 0)}

    bug, place = movestr.split()

    move, bug2 = place_to_relative(place)

    if bug2 not in board:
        raise ValueError(f"Bug {bug2} requested in '{movestr}' not placed yet")

    newboard = {}

    for b, p in board.items():
        if b != bug:
            newboard[b] = p

    newboard[bug] = tuple(x + y for x, y in zip(board[bug2], move, strict=True))  # type: ignore

    return newboard


def add_movelist(movelist: list[str], board: Optional[Board] = None) -> list[Board]:
    """Add several moves to a board and return the new board."""
    if board is None:
        board = {}
    return list(accumulate(movelist, add_move, initial=board))


def add_moves(moves: str, board: Optional[Board] = None) -> list[Board]:
    """Add several moves to a board and return the new board."""
    if board is None:
        board = {}
    return add_movelist(moves.split(";"), board)


def place_to_relative(place: str) -> tuple[tuple[int, int, int], str]:
    """Unpack a place to a move and a bug."""
    if place.endswith("/"):
        move = (1, -1, 0)
        bug2 = place[:-1]
    elif place.endswith("-"):
        move = (1, 0, 0)
        bug2 = place[:-1]
    elif place.endswith("\\"):
        move = (0, 1, 0)
        bug2 = place[:-1]
    elif place.startswith("/"):
        move = (-1, 1, 0)
        bug2 = place[1:]
    elif place.startswith("-"):
        move = (-1, 0, 0)
        bug2 = place[1:]
    elif place.startswith("\\"):
        move = (0, -1, 0)
        bug2 = place[1:]
    else:
        move = (0, 0, 1)
        bug2 = place

    return move, bug2


def hex_to_rectangular(
    pair: tuple[str, tuple[int, int, int]]
) -> tuple[str, tuple[int, int, int]]:
    """Map an axial hex representation to Cartesian coordinates."""
    bug, position = pair
    x = 4 * position[0] + 2 * position[1]
    y = position[1]
    z = position[2]
    return (bug, (x, y, z))


def pretty_print_board(board: Board):
    """Pretty-print a board."""
    pretty_print_lboard(sorted(board.items()))


def pretty_print_lboard(board: LBoard):
    """Pretty-print a board."""
    cartesian = sorted(
        map(hex_to_rectangular, board),
        key=lambda t: (t[1][1], t[1][0], -t[1][2]),
    )
    minx = min((t[1][0] for t in cartesian))
    oldx, oldy = minx, cartesian[0][1][1]
    for bug, (x, y, _) in cartesian:
        if y == oldy and x == oldx - 4:
            continue
        if y != oldy:
            print("\n\n", end="")
            oldx = minx
            oldy = y
        print(" " * (x - oldx), end="")
        print(f"{bug:4}", end="")
        oldx = x + 4
    print()


def rotate_board(board: Board) -> LBoard:
    """Rotate a board so that bQ is at ESE."""
    if len(board) <= 1:
        return sorted(board.items())

    if "bQ" not in board:
        # FIXME there might rarely be an ambiguity there
        refbug = sorted(board.keys())[0]
    else:
        refbug = "bQ"

    x, y, _ = board[refbug]

    i = 0
    while x <= 0 or y < 0:
        i += 1
        x, y = x + y, -x

    newboard = board
    for _ in range(i):
        for e in board:
            x, y, z = newboard[e]
            newboard[e] = x + y, -x, z

    # We also remove bug numbers at this stage
    return sorted([(bug[:2], (x, y, z)) for bug, (x, y, z) in newboard.items()])


def flip_board(board: Board) -> Board:
    """Flip a board so that bQ is at ESE."""
    return {bug: (y, x, z) for bug, (x, y, z) in board.items()}


def center_board(board: Board) -> Board:
    """Center a board on the wQ."""
    if "wQ" not in board:
        # FIXME there might rarely be an ambiguity there
        refbug = sorted(board.keys())[-1]
    else:
        refbug = "wQ"

    x, y, _ = board[refbug]

    newboard = {}
    for bug, (bx, by, bz) in board.items():
        newboard[bug] = (bx - x, by - y, bz)

    return newboard


def normalize_board(board: Board) -> LBoard:
    """Center, rotate and flip board."""
    cboard = center_board(board)
    board1 = rotate_board(cboard)
    # flipping after rotation almost works, but fails on refbug at (0, y)
    board2 = rotate_board(flip_board(cboard))

    if board1 <= board2:
        return board1
    return board2


def hash_board(board: Board) -> bytes:
    """Return the hash value of a normalized board."""
    newboard = normalize_board(board)
    return compress(
        repr(newboard)
        .translate({ord(i): None for i in (" ", "(", ")", "'", "[", "]", "0")})
        .encode()
    )
