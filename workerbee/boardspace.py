"""Analyzing databases of Hive games.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from typing import Dict, Optional


def parse_boardspace(
    filename: str,
    filedata: Optional[str] = None,
) -> tuple[str, str, str, str, list[str]]:
    """Return a parsed BoardSpace.net 'SGF' file."""
    if filedata is None:
        with open(filename) as f:
            filedata = f.read()
    lines = filedata.splitlines()
    moves: list[str] = []
    positions: Dict[str, list[str]] = {}
    oldpos = None
    result = "?"

    for line in lines:
        line = line.lstrip("; ")
        if line.startswith("RE["):
            result = line.split()[-1][:-1]
        elif line.startswith("P0[id ") or line.startswith("P0[ id "):
            white = line.split('"')[1]
        elif line.startswith("P1[id ") or line.startswith("P1[ id "):
            black = line.split('"')[1]
        elif line.startswith("P"):
            if "Dropb" in line or "dropb" in line or "Move" in line or "move" in line:
                handle_drop(line, oldpos, positions, moves)
            elif "Pickb" in line or "pickb" in line:
                oldpos = handle_pick(line, oldpos, positions)
            elif "Pass" in line:
                moves.append("pass")
            elif "AcceptDraw" in line:
                result = "D"
            # TODO add the Resign case?

    return (
        white,
        black,
        normalize_result(result, white, black),
        date_from_filename(filename),
        moves,
    )


def date_from_filename(filename: str) -> str:
    """Extract the date from the filename."""
    # the date field in SGF is a mess
    date_tokens = filename.split(".")[-2].split("-")
    return f"{date_tokens[-4]}-{date_tokens[-3]}-{date_tokens[-2]} {date_tokens[-1][:2]}:{date_tokens[-1][2:]}"


def normalize_result(result: str, white: str, black: str) -> str:
    """Turn player name in result into proper W/B thing."""
    if result not in ("D", "?"):
        if white in result:
            return "W"
        elif black in result:
            return "B"
        else:
            return "D"
    return result


def normalize_bug(bug: str, context: str) -> str:
    """Normalize BS bug name."""
    # old files have bugs without color, so b1 is a bug…
    if len(bug) < 2 or not bug[1].isalpha():
        if "P0" in context:
            nbug = "w" + bug
        else:
            nbug = "b" + bug
    else:
        nbug = bug
    nbug = nbug[0] + nbug[1:2].upper() + nbug[2:]
    if nbug[1] in ("P", "L", "M") and len(nbug) == 3:
        nbug = nbug[:-1]
    return nbug


def normalize_move(move: str) -> str:
    """Normalize BS move."""
    return move.replace("P1", "P").replace("L1", "L").replace("M1", "M")


def handle_pick(
    line: str, oldpos: Optional[str], positions: Dict[str, list[str]]
) -> str:
    """Note Pickb position."""
    tokens = line.split()
    oldpos = f"{tokens[2]} {tokens[3]}"
    positions[oldpos].pop()
    return oldpos


def handle_drop(
    line: str, oldpos: Optional[str], positions: Dict[str, list[str]], moves: list[str]
) -> None:
    """Move a bug."""
    tokens = line.split()
    for m in ("Move", "move", "PMove", "pmove"):
        if m in tokens:
            tokens.remove(m)
    bug = normalize_bug(tokens[2], tokens[0])
    newpos = f"{tokens[3]} {tokens[4]}"
    move = tokens[5].split("]")[0].replace("\\\\", "\\")
    if move == ".":
        if newpos not in positions or not positions[newpos]:
            positions[newpos] = [bug]
            if oldpos is None:
                moves.append(bug)
            else:
                bug2 = normalize_bug(positions[oldpos][-1], "")
                moves.append(build_move(bug, bug2, oldpos, newpos))
        else:
            moves.append(f"{bug} {positions[newpos][-1]}")
            positions[newpos].append(bug)
    else:
        moves.append(f"{bug} {normalize_move(move)}")
        if newpos in positions:
            positions[newpos].append(bug)
        else:
            positions[newpos] = [bug]


def build_move(bug: str, bug2: str, oldpos: str, newpos: str) -> str:
    """Create a relative move from an absolute one."""
    newx, newy = newpos.split()
    oldx, oldy = oldpos.split()
    if oldx == newx:
        if int(newy) > int(oldy):
            return f"{bug} \\{bug2}"
        return f"{bug} {bug2}\\"
    elif oldx < newx:
        if oldy == newy:
            return f"{bug} {bug2}-"
        return f"{bug} {bug2}/"
    else:
        if oldy == newy:
            return f"{bug} -{bug2}"
        return f"{bug} /{bug2}"


def bs_to_entomology(filename: str, move: int) -> str:
    """Give the entomology URL for this file."""
    return f"https://entomology.gitlab.io/hive.html?color=1&game=bsn:{filename[:-4]}&move={move}"
