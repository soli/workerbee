"""Tests for Bee.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from workerbee import bee, boardspace


def test_parsing_bot():
    """Test some SGF parsing."""
    (w, b, r, d, m) = boardspace.parse_boardspace("test/bot-2023-08-11-1920.sgf")
    print(m)
    board = bee.add_movelist(m)[-1]
    bee.pretty_print_board(board)
    assert w == "SHLOMO"
    assert b == "WeakBot"
    assert r == "B"
    assert d == "2023-08-11 19:20"


def test_parsing_pass():
    """Test some SGF parsing."""
    (w, b, r, d, m) = boardspace.parse_boardspace("test/pass-2023-08-11-1050.sgf")
    print(m)
    board = bee.add_movelist(m)[-1]
    bee.pretty_print_board(board)
    assert w == "ringersoll"
    assert b == "Quodlibet"
    assert r == "B"
    assert d == "2023-08-11 10:50"


def test_parsing_draw():
    """Test some SGF parsing."""
    (w, b, r, d, m) = boardspace.parse_boardspace("test/draw-2023-08-12-1903.sgf")
    print(m)
    board = bee.add_movelist(m)[-1]
    bee.pretty_print_board(board)
    assert w == "stepanzo"
    assert b == "MaxShark"
    assert r == "D"
    assert d == "2023-08-12 19:03"


def test_parsing_foo():
    """Test some SGF parsing."""
    (w, b, r, d, m) = boardspace.parse_boardspace("test/foo-2023-08-13-1221.sgf")
    print(m)
    board = bee.add_movelist(m)[-1]
    bee.pretty_print_board(board)
    assert w == "MaxShark"
    assert b == "hive2"
    assert r == "B"
    assert d == "2023-08-13 12:21"


def test_parsing_bar():
    """Test some SGF parsing."""
    (w, b, r, d, m) = boardspace.parse_boardspace("test/bar-2020-11-15-1751.sgf")
    print(m)
    board = bee.add_movelist(m)[-1]
    bee.pretty_print_board(board)
    assert w == "stepanzo"
    assert b == "shturik"
    assert r == "W"
    assert d == "2020-11-15 17:51"
