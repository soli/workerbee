"""Tests for Bee.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from pytest import raises
from workerbee import bee


def test_add_first_move():
    """Test successful first move."""
    assert(sorted(bee.add_move({}, "wP").items()) == [("wP", (0, 0, 0))])


def test_add_nonexistent_relative():
    """Test successful first move."""
    with raises(ValueError):
        bee.add_move({"wP": (0, 0, 0)}, "bP wQ-").items()


def test_add_nonexistent_position():
    """Test successful first move."""
    with raises(ValueError):
        bee.add_move({"wP": (0, 0, 0)}, "bP wP?").items()


def test_add_second_move1():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wP"), "bM wP-").items()) == [("bM", (1, 0, 0)), ("wP", (0, 0, 0))]


def test_add_second_move2():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wQ"), "bP wQ/").items()) == [("bP", (1, -1, 0)), ("wQ", (0, 0, 0))]


def test_add_second_move3():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wL"), "bL wL\\").items()) == [("bL", (0, 1, 0)), ("wL", (0, 0, 0))]


def test_add_second_move4():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wH1"), "bH1 -wH1").items()) == [("bH1", (-1, 0, 0)), ("wH1", (0, 0, 0))]


def test_add_second_move5():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wB1"), "bM /wB1").items()) == [("bM", (-1, 1, 0)), ("wB1", (0, 0, 0))]


def test_add_second_move6():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wM"), "bA1 \\wM").items()) == [("bA1", (0, -1, 0)), ("wM", (0, 0, 0))]


def test_add_second_move7():
    """Test successful second move."""
    assert sorted(bee.add_move(bee.add_move({}, "wM"), "bB1 wM").items()) == [("bB1", (0, 0, 1)), ("wM", (0, 0, 0))]


def test_pretty_print_rotate(capsys):
    """Rotate board."""
    board = bee.add_moves("wL;bL /wL;wQ wL-;bQ -bL")[-1]
    bee.pretty_print_lboard(bee.rotate_board(bee.center_board(board)))
    captured = capsys.readouterr()
    assert captured.out == "wQ  \n\n  wL  bL  \n\n        bQ  \n"


def test_pretty_print_flip(capsys):
    """Flip board."""
    board = bee.add_moves("wL;bL /wL;wQ wL-;bQ -bL")[-1]
    bee.pretty_print_lboard(bee.rotate_board(bee.flip_board(bee.center_board(board))))
    captured = capsys.readouterr()
    assert captured.out == "wQ  wL  \n\n      bL  bQ  \n"


def test_normalize():
    """Board normalization."""
    board = bee.add_moves("wL;bL /wL;wQ wL-;bQ -bL")[-1]
    assert bee.normalize_board(board) == [('bL', (1, 1, 0)), ('bQ', (1, 2, 0)), ('wL', (0, 1, 0)), ('wQ', (0, 0, 0))]


def test_hash():
    """Board hashing."""
    board = bee.add_moves("wL;bL /wL;wQ wL-;bQ -bL")[-1]
    assert bee.hash_board(board) == b"x\x9cK\xf2\xd11\x04B\x9d\xa4@ i\xa4\xa3S\xee\xa3\x03\xe2\x96\x07\xea\xe8\xe8\x00\x00^\xd9\x06w"
