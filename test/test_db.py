"""Tests for Bee.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from os import rename

from workerbee import db


def test_add_to_db(capsys):
    """Test adding a file to the DB."""
    try:
        rename("data/games.db", "data/games.db.orig")
        renamed = True
    except FileNotFoundError:
        renamed = False
    db.add_bs_to_db("test/bot-2023-08-11-1920.sgf")
    db.add_bs_to_db("test/pass-2023-08-11-1050.sgf")
    db.add_bs_to_db("test/draw-2023-08-12-1903.sgf")
    db.add_bs_to_db("test/draw-2023-08-12-1903.sgf")
    captured = capsys.readouterr()
    rename("data/games.db", "data/games.db.test")
    if renamed:
        rename("data/games.db.orig", "data/games.db")
    assert captured.out.splitlines()[-1] == "test/draw-2023-08-12-1903.sgf is already in the database."


def test_query_db(capsys):
    """Query our test DB."""
    result = db.query("wP;bL wP\\;wM \\wP;bQ bL-;wQ -wP;bP bQ-;wA1 wM-")
    assert len(result) > 3
