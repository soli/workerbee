"""Web interface for Bee.

Copyright (C) 2023 Sylvain.Soliman@m4x.org

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from collections import Counter

from bottle import default_app, get, post, request

from workerbee import bs_to_entomology, count_games, query

CSS = """
<style>
body {
  color: #555;
  margin: 0 auto;
  max-width: 50em;
  font-family: "Helvetica", "Arial", sans-serif;
  line-height: 1.5;
  padding: 2em 1em;
}
code {
  background: #eee;
  padding: 2px 4px;
  vertical-align: text-bottom;
}
input {
  padding: 5px;
  margin: 5px;
}
table {
  border-collapse: collapse;
  table-layout: fixed;
  text-align: left;
  overflow: hidden;
  box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
  margin: auto;
  margin-top: 50px;
  margin-bottom: 50px;
  border-radius: 5px;
}
table th {
  background-color: #ff9800;
  color: #fff;
  font-weight: bold;
  padding: 10px;
  text-transform: uppercase;
  letter-spacing: 1px;
  border: 0px;
  border-top: 1px solid #fff;
  border-bottom: 1px solid #ccc;
}
table tr:nth-child(even) td {
  background-color: #f2f2f2;
}

table tr:hover td {
  background-color: #ffedcc;
}
table td {
  background-color: #fff;
  padding: 10px;
  border-bottom: 1px solid #ccc;
}
.box {
  width: calc(1440px * 0.2);
  height: calc(900px * 0.2);
  background-color: #bbb;
  -ms-zoom: 0.2;
  -moz-transform: scale(0.2);
  -moz-transform-origin: 0 0;
  -o-transform: scale(0.2);
  -o-transform-origin: 0 0;
  -webkit-transform: scale(0.2);
  -webkit-transform-origin: 0 0;
}
.box iframe {
  width: 1440px;
  height: 900px;
  border: 0px;
}
.box:after {
  content: "";
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
}
.box-container {
  width: calc(1440px * 0.2);
  height: calc(900px * 0.2);
  display: inline-block;
  overflow: hidden;
  position: relative;
}
</style>
"""

JS = """
"""

@get("/")
def get_query():
    """Get query."""
    if request.params.get("query"):
        return get_result()
    return "<html><head>" + CSS + """
        </head><body>
        <h2><a href="https://www.worldhivetournaments.com/home">Hive®</a> tournament games query server</h2>
        <h3>Notice: something broke in my code and I won't have time to fix it, so this server will soon disappear, if interested you can get and fix the code on Codeberg</h3>
        <div>
        <p>Enter a position as a sequence of moves in <a
        href="https://github.com/jonthysell/Mzinga/wiki/UniversalHiveProtocol#user-content-movestring">UHP</a>
        notation, separated by semicolons.</p>
        <p>The sequence need not be legal but should obtain the proper position.
        E.g. <code>wL;bL wL-;wA1 -wL;bQ bL\;wQ \wL</code></p>
        <p>Enter optionally more constraints if wanted (dates, players…)</p>
        <p>You will get all games (from <a href="http://boardspace.net/">BoardSpace</a>, marked as "Tournament",
        that were successfully parsed, and where that position occurred (with symmetries/rotations/move order factored out)</p>
        <p>Games are grouped by their next move and each provide an <a href="https://entomology.gitlab.io/">entomology</a> link
        (that might not work if the game is actually not in their DB yet)</p>
        </div>
        <form action="/" method="get">
            <input name="query" type="text" size="65" placeholder="Query (less than 32 moves)"><br>
            <input name="fromdate" type="text" size="30" placeholder="Optional from date (e.g. 2000-01-01)">
            <input name="todate" type="text" size="30" placeholder="Optional to date (e.g. 2030-01-01)"><br>
            <input name="white" type="text" size="30" placeholder="Optional white player">
            <input name="black" type="text" size="30" placeholder="Optional black player"><br>
            <input value="Send" type="submit">
            <input type="reset">
        </form></body></html>
    """


@post("/")
def get_result():
    """Get result of query."""
    q = request.params.get("query")
    if not q:
        return "The query cannot be empty."
    fromdate = request.params.get("fromdate")
    if not fromdate:
        fromdate = None
    todate = request.params.get("todate")
    if not todate:
        todate = None
    white = request.params.get("white")
    if not white:
        white = None
    black = request.params.get("black")
    if not black:
        black = None

    try:
        result = query(q, fromdate, todate, white, black)
        counter = Counter()
        for _, _, r, _ in result:
            counter[r] += 1
        output = "<html><head>" + CSS + JS + "</head>\n<body>\n"
        caption = f'{len(result)} results for query "{q}"<br/>'
        if counter.total() > 0:
            caption += f"""{counter['W']} W [{round(100 * counter['W']/counter.total())}%],
            {counter['B']} B [{round(100 * counter['B']/counter.total())}%],
            {counter['D']} D [{round(100 * counter['D']/counter.total())}%]"""
        if fromdate:
            caption += f' from "{fromdate}"'
        if todate:
            caption += f' to "{todate}"'
        if white:
            caption += f" with {white} as W"
        if black:
            caption += f" with {black} as B"
        caption += f" over {count_games()} games<p>\n"
        output += "<table><caption>\n" + caption + "</caption>\n"
        output += "<thead><tr><th>Result</th><th>Game</th>"
        output += "<th>Next move</th></tr></thead>\n<tbody>\n"
        previoushash = None
        for filename, move, r, nexthash in result:
            entomology = bs_to_entomology(filename, move)
            nextent = bs_to_entomology(filename, move + 1)
            output += f'<tr><td>{r}</td><td><a href="{entomology}">{filename}</a></td>\n'
            if nexthash != previoushash:
                previoushash = nexthash
                output += f'<td><div class="box-container"><div class="box" onClick="this.firstElementChild.src=\'{nextent}\';"><iframe loading="lazy" sandbox="allow-scripts allow-same-origin" ></iframe></div></div></td>'
            else:
                output += "<td>Same as above</td>"
            output += "</tr>\n"
        output += "</tbody>\n</table>\n"
        output += '<div style="text-align: center;">Games visualization (click the gray rectangle) thanks to <a href="https://entomology.gitlab.io/">Entomology\'s Game Viewer</a></div>\n'
        output += "</body></html>"
        return output
    except Exception as e:
        return f"There was an error with your query: {e}"


application = default_app()
